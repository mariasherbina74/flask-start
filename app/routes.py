from app import app
from datetime import datetime
from flask import render_template
from flask import request
from markupsafe import escape
import math
import feedparser
import more_itertools as mit

@app.route('/')
@app.route('/index')
def index():

    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/news', methods=['GET','POST'])
def news():
    if request.method == 'GET':
      y = 'http://news.yandex.ru/Russia/index.rss'
    if request.method == 'POST':
     frm=request.form 
     y = f'http://news.yandex.ru/{frm["text"]}/index.rss'

    d = feedparser.parse(y)
    entries = d['entries'][0:8] 
    cols=3 
    r=mit.chunked(d, cols)

    if len(entries)==0:
        return_template('404.html')


    return render_template(
        'news.html',
        title='Новости',
        cols=cols,
        data=entries,
        rows=math.ceil(len(entries)/cols),
        r=r
    )

@app.route('/about')
def about():
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.',
        username='')

@app.route('/contact')
def contact():
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Моя contact page.',
        text="hello"
    )

@app.route('/file')
def file1():
    return render_template(
        'file.html',
        title='FILE',
        year=datetime.now().year,
        message='Новый файл.',
        s='//HELLO'
    )
