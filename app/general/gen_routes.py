from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,jsonify, current_app, after_this_request, send_from_directory,send_file
)
from flask import session as ses
from app import db
import feedparser
from werkzeug.utils import secure_filename
import zipfile
import shutil
import os,sys


bp_gen = Blueprint('general', __name__, template_folder='templates')
print('name - general',__name__)

@bp_gen.route('/')
@bp_gen.route('/index')
def index():
    return render_template(
    '/general/index.html',
    title='Home Page',
       )

@bp_gen.route('/getdata')
def get_data():
    text = request.args.get('txt', 0, type=str)

    if text.lower() == 'python':
        return jsonify(result='Выбран правильный язык')
    else:
	    return jsonify(result='Попробуйте еще')


@bp_gen.route('/session')
def session():
  return render_template(
    '/general/session.html',
    title='Сессия',
    )


@bp_gen.route('/getsession')
def get_session():
  if request.method == 'GET':
    data={
      'fam':request.args.get('fam'),
      'age': request.args.get('age'),
      'session':[{'Математика':5,'дата':'08.04.2022','преподаватель':' Иванов А.В'},
                  {'Экономика':4},
                  {'Информатика':5}]
    }

    print(data)

    return jsonify(data)   
    
      
@bp_gen.route('/carusel')
def owl():

    if request.method == 'GET':
        ya ='http://news.yandex.ru/Russia/index.rss'

    data = feedparser.parse(ya)
    entr = data['entries']
    
    return render_template(
        '/general/owl.html',
        title='Карусель',
        data=entr
    )

@bp_gen.route('/uploadfile')
def file():
    @after_this_request
    def clearmessage(response):
      ses['k']=''
      ses['l']=''
      return response
      
    return render_template(
      '/general/upload.html',
        title='Выгрузка файла',
        message=ses.get('k', ''),
        m=ses.get('l','')
    )    

@bp_gen.route('/savefile',methods=['POST'])
def savefile():
    if request.method == 'POST':
      #print(request.files)

      if 'datafile' not in request.files:
          return redirect(request.origin+url_for('general.file',message="Сохранение файла невозможно"))

      file = request.files['datafile']
      fls =  request.files.getlist("datafile") 
      # filename = secure_filename(file.filename)
    
      fileNameList=[]

      for f in fls:
       
        fileNameList.append(f.filename)
        print(dir(f))
        print("filename",f.filename)
        fileRead=f.read()
        print('LEN ',len(fileRead))        
        if len(fileRead)>=27000:
          errorList()
          return redirect(request.origin+url_for('general.file',m="Большой размер файла. Сохранение невозможно"))
        
        f.save(os.path.join(current_app.config['UPLOAD_FOLDER'],f.filename))
      
      ses['k']=fileNameList
 
    #   return redirect(request.origin+url_for('general.file',message="Файл "+ filename+ " сохранен"))
      files=os.listdir(current_app.config['UPLOAD_FOLDER'])
    
      return redirect(request.origin+url_for('general.file'))

def errorList():
  fls =  request.files.getlist("datafile") 
  for f in fls:
     fileRead=f.read()
     ses['l']="Большой размер файла "+ f.filename
     

@bp_gen.route('/downloadfile')
def load():
  files=os.listdir(current_app.config['UPLOAD_FOLDER'])   
  return render_template(
        '/general/download.html',
        title='Загрузка файла',
        files=files
    ) 


@bp_gen.route('/uploads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'], filename ,as_attachment=True)


@bp_gen.route('/downfiles')
def downfiles():

    shutil.make_archive(current_app.config['TMP_FOLDER'] +'/'+'ZIPFiles','zip',current_app.config['UPLOAD_FOLDER'])
    return send_file(current_app.config['TMP_FOLDER'] +'/'+'ZIPFiles.zip',
            mimetype = 'zip',
            attachment_filename=  'ZIPFiles.zip',
            as_attachment = True)

